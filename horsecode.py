import argparse

from PIL import Image


MORSE_CODE = {
        'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.',
        'G': '--.', 'H': '....', 'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..',
        'M': '--', 'N': '-.', 'O': '---', 'P': '.--.', 'Q': '--.-', 'R': '.-.',
        'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-',
        'Y': '-.--', 'Z': '--..'
        }

DOT = Image.open('dot.png')
DASH = Image.open('dash.png')

DOT_WIDTH, DOT_HEIGHT = DOT.size
DASH_WIDTH, DASH_HEIGHT = DASH.size
SPACE_WIDTH = 100

if DOT_HEIGHT >= DASH_HEIGHT:
    HEIGHT = DOT_HEIGHT
else:
    HEIGHT = DASH_HEIGHT

def letters_to_morse_code(letters):
    morse_code = []
    for letter in letters.upper():
        try:
            morse_code.append(MORSE_CODE[letter])
        except KeyError:
            continue

    return ' '.join(morse_code)


def morse_code_to_horse_code(morse_code):
    dot_count, dash_count, space_count = 0, 0, 0
    for symbol in morse_code:
        if symbol == '-':
            dash_count += 1
        elif symbol == '.':
            dot_count += 1
        else:
            space_count += 1
    image_width = DOT_WIDTH * dot_count + DASH_WIDTH * dash_count + SPACE_WIDTH * space_count 

    horse_code = Image.new('RGB', (image_width, HEIGHT), (255,255,255))

    pos = 0
    for symbol in morse_code:
        if symbol == '-':
            horse_code.paste(DASH, (pos, 0))
            pos += DASH_WIDTH
        elif symbol == '.':
            horse_code.paste(DOT, (pos, 0))
            pos += DOT_WIDTH
        else:
            pos += SPACE_WIDTH

    return horse_code


def letters_to_horse_code(letters):
    morse_code = letters_to_morse_code(letters)
    return morse_code_to_horse_code(morse_code)



def main(letters, output_filename):
    horse_code = letters_to_horse_code(letters)
    horse_code.save(output_filename)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('text', type=str, help='Text to convert to horse code.')
    parser.add_argument('output', type=str, help='Output filename')
    args = parser.parse_args()
    main(args.text, args.output)
